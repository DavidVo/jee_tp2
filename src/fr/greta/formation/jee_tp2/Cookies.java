package fr.greta.formation.jee_tp2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Cookies
 */
@WebServlet(name ="Cookie", urlPatterns= "/Cookies")
public class Cookies extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cookies() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Cookie monCookie = new Cookie("Premier_cle", "Premiere_Value");
		monCookie.setMaxAge(3600);
		response.addCookie(monCookie);
		

        PrintWriter printOut = response.getWriter();
        printOut.println("<html lang=\"fr\">");
        printOut.println("<head>");
        printOut.println("<meta charset=\"utf-8\">");
        printOut.println("</head>");
        printOut.println("<body>");
        printOut.println("<div class=\"container\">");
        printOut.println("<p> Creation Cookie ?</p>");
        printOut.println("</div>");
        printOut.println("</body>");
        printOut.println("</html>");
        printOut.flush();
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
