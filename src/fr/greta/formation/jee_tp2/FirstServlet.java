package fr.greta.formation.jee_tp2;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstServlet
 */
@WebServlet(name="FirstServlet", urlPatterns="/FirstServlet")
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FirstServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        // TODO Auto-generated method stub
        //response.setContentType("plain/text");
        //PrintWriter lOut = response.getWriter();

        response.setContentType("text/html");
        PrintWriter printOut = response.getWriter();
        printOut.println("<html lang=\"fr\">");
        printOut.println("<head>");
        printOut.println("<meta charset=\"utf-8\">");
        printOut.println("</head>");
        printOut.println("<body>");
        printOut.println("<div class=\"container\">");
        printOut.println("<h1>Titre 1</h1>");
        printOut.println("<p>Un paragraphe</p>");
        printOut.println("</div>");
        printOut.println("</body>");
        printOut.println("</html>");
        printOut.flush();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
