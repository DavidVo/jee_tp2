package fr.greta.formation.jee_tp2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Formulaire
 */
@WebServlet(name="Formulaire", urlPatterns="/Formulaire")
public class Formulaire extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Formulaire() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String sex;
		String nomValue = request.getParameter("nom");
		String prenomValue = request.getParameter("prenom");
		boolean sexValue= Boolean.parseBoolean(request.getParameter("sexe")); 
		if (sexValue) {sex = "Madame";}
		else {sex = "Monsieur";}
		

        response.setContentType("text/html");
        PrintWriter printOut = response.getWriter();
        printOut.println("<html lang=\"fr\">");
        printOut.println("<head>");
        printOut.println("<meta charset=\"utf-8\">");
        printOut.println("</head>");
        printOut.println("<body>");
        printOut.println("<div class=\"container\">");
        printOut.println("<p> Bonjour " + sex + " " + prenomValue + ", puis-je vous appeler " +nomValue +" ?</p>");
        printOut.println("</div>");
        printOut.println("</body>");
        printOut.println("</html>");
        printOut.flush();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
